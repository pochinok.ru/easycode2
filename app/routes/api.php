<?php

use App\Http\Controllers\Settings\SettingsController;
use App\Http\Controllers\User\AuthController;
use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('/auth')->group(function () {
    Route::post('/registration', [AuthController::class, 'registration']);
    Route::post('/login', [AuthController::class, 'login']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/user')->group(function () {
        Route::get('/me', [UserController::class, 'me']);
        Route::post('/update/{id}', [UserController::class, 'update']);
        Route::post('/delete/{id}', [UserController::class, 'delete']);
    });

    Route::prefix('/settings')->group(function () {
        Route::get('/list', [SettingsController::class, 'list']);
        Route::get('/byId/{id}', [SettingsController::class, 'byId']);
        Route::post('/create', [SettingsController::class, 'create']);
        Route::post('/userBinding/{id}', [SettingsController::class, 'userBinding']);
        Route::post('/update/{id}', [SettingsController::class, 'update']);
        Route::post('/confirm/{id}', [SettingsController::class, 'confirm']);
        Route::post('/delete/{id}', [SettingsController::class, 'delete']);
    });


});
