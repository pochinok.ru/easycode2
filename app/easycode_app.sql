-- Adminer 4.8.1 PostgreSQL 15.1 (Debian 15.1-1.pgdg110+1) dump

DROP TABLE IF EXISTS "failed_jobs";
DROP SEQUENCE IF EXISTS failed_jobs_id_seq;
CREATE SEQUENCE failed_jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."failed_jobs" (
    "id" bigint DEFAULT nextval('failed_jobs_id_seq') NOT NULL,
    "uuid" character varying(255) NOT NULL,
    "connection" text NOT NULL,
    "queue" text NOT NULL,
    "payload" text NOT NULL,
    "exception" text NOT NULL,
    "failed_at" timestamp(0) DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "failed_jobs_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "failed_jobs_uuid_unique" UNIQUE ("uuid")
) WITH (oids = false);


DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" integer DEFAULT nextval('migrations_id_seq') NOT NULL,
    "migration" character varying(255) NOT NULL,
    "batch" integer NOT NULL,
    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "migrations" ("id", "migration", "batch") VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_reset_tokens_table',	1),
(3,	'2019_08_19_000000_create_failed_jobs_table',	1),
(4,	'2019_12_14_000001_create_personal_access_tokens_table',	1),
(5,	'2023_10_25_155238_create_settings_table',	2),
(6,	'2023_10_25_155253_create_users_settings_table',	2),
(7,	'2023_10_25_155401_create_confirmation_table',	2),
(8,	'2023_10_27_070427_update_users_table',	3);

DROP TABLE IF EXISTS "password_reset_tokens";
CREATE TABLE "public"."password_reset_tokens" (
    "email" character varying(255) NOT NULL,
    "token" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    CONSTRAINT "password_reset_tokens_pkey" PRIMARY KEY ("email")
) WITH (oids = false);


DROP TABLE IF EXISTS "personal_access_tokens";
DROP SEQUENCE IF EXISTS personal_access_tokens_id_seq;
CREATE SEQUENCE personal_access_tokens_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."personal_access_tokens" (
    "id" bigint DEFAULT nextval('personal_access_tokens_id_seq') NOT NULL,
    "tokenable_type" character varying(255) NOT NULL,
    "tokenable_id" bigint NOT NULL,
    "name" character varying(255) NOT NULL,
    "token" character varying(64) NOT NULL,
    "abilities" text,
    "last_used_at" timestamp(0),
    "expires_at" timestamp(0),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "personal_access_tokens_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "personal_access_tokens_token_unique" UNIQUE ("token")
) WITH (oids = false);

CREATE INDEX "personal_access_tokens_tokenable_type_tokenable_id_index" ON "public"."personal_access_tokens" USING btree ("tokenable_type", "tokenable_id");

INSERT INTO "personal_access_tokens" ("id", "tokenable_type", "tokenable_id", "name", "token", "abilities", "last_used_at", "expires_at", "created_at", "updated_at") VALUES
(1,	'App\Models\User',	8,	'login',	'845f5588b92d717de09652b24a44b6fdf38dd29b1488cf37222fe2862e06e197',	'["*"]',	'2023-10-27 07:08:42',	NULL,	'2023-10-25 19:11:02',	'2023-10-27 07:08:42');

DROP TABLE IF EXISTS "settings";
DROP SEQUENCE IF EXISTS settings_id_seq;
CREATE SEQUENCE settings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."settings" (
    "id" bigint DEFAULT nextval('settings_id_seq') NOT NULL,
    "title" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "settings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "settings" ("id", "title", "created_at", "updated_at") VALUES
(2,	'Премиум статус',	'2023-10-25 19:50:20',	'2023-10-25 19:50:20'),
(3,	'Неограниченный просмотр',	'2023-10-25 19:50:42',	'2023-10-25 19:50:42'),
(4,	'Большая документация',	'2023-10-25 19:51:02',	'2023-10-25 19:51:02');

DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "email" character varying(255) NOT NULL,
    "email_verified_at" timestamp(0),
    "password" character varying(255) NOT NULL,
    "remember_token" character varying(100),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "phone_number" character varying(255),
    CONSTRAINT "users_email_unique" UNIQUE ("email"),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "users" ("id", "name", "email", "email_verified_at", "password", "remember_token", "created_at", "updated_at", "phone_number") VALUES
(9,	'anya',	'ochinok.ru@gmail.com',	NULL,	'$2y$10$mz.Rhaqt58aS2rdnKtKc2un.k2hMPV2wK8EAMph43Hi.YeabjwnEG',	NULL,	'2023-10-25 19:10:30',	'2023-10-25 19:10:30',	NULL),
(11,	'Sanya',	'chinok.ru@gmail.com',	NULL,	'$2y$10$mz.Rhaqt58aS2rdnKtKc2un.k2hMPV2wK8EAMph43Hi.YeabjwnEG',	NULL,	'2023-10-25 19:10:30',	'2023-10-25 19:10:30',	NULL),
(8,	'Danya',	'pochinok.ru@gmail.com',	NULL,	'$2y$10$mz.Rhaqt58aS2rdnKtKc2un.k2hMPV2wK8EAMph43Hi.YeabjwnEG',	NULL,	'2023-10-25 19:10:30',	'2023-10-25 19:10:30',	'89676628407');

DROP TABLE IF EXISTS "users_settings";
DROP SEQUENCE IF EXISTS users_settings_id_seq;
CREATE SEQUENCE users_settings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."users_settings" (
    "id" bigint DEFAULT nextval('users_settings_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "setting_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "users_settings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "users_settings" ("id", "user_id", "setting_id", "created_at", "updated_at") VALUES
(38,	8,	4,	'2023-10-25 21:31:07',	'2023-10-25 21:31:07'),
(39,	9,	4,	'2023-10-25 21:31:07',	'2023-10-25 21:31:07'),
(40,	11,	4,	'2023-10-25 21:31:07',	'2023-10-25 21:31:07');

ALTER TABLE ONLY "public"."users_settings" ADD CONSTRAINT "users_settings_setting_id_foreign" FOREIGN KEY (setting_id) REFERENCES settings(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."users_settings" ADD CONSTRAINT "users_settings_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE NOT DEFERRABLE;

-- 2023-10-27 07:09:24.121995+00
