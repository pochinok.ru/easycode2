<?php

namespace App\Resources\Settings;

use App\Resources\User\UserListResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SettingsByIdResource extends JsonResource
{
    /**
     * @param Request|null $request
     * @return array
     */
    public function toArray(Request $request = null): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'users' => new UserListResource($this->user),
            'createdAt' => $this->created_at,
        ];
    }
}


