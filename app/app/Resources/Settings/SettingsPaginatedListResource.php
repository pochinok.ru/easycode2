<?php

namespace App\Resources\Settings;

use App\Resources\User\UserListResource;
use App\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class SettingsPaginatedListResource extends JsonResource
{
    /**
     * @param Request|null $request
     * @return array
     */
    public function toArray(Request $request = null): array
    {
        $items = [];
        /** @var LengthAwarePaginator $paginator */
        $paginator = $this->getSettings();

        foreach ($paginator->items() as $setting) {
            $items[] = [
                'id' => $setting->id,
                'title' => $setting->name,
                'users' => new UserListResource($setting->user),
                'createdAt' => $setting->created_at,
            ];
        }

        return (
        new LengthAwarePaginator(
            $items,
            $paginator->total(),
            $paginator->perPage(),
            $paginator->currentPage()
        )
        )
            ->toArray();
    }

    public function getSettings()
    {
        return $this->resource;
    }
}

