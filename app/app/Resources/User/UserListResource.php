<?php

namespace App\Resources\User;

use App\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class UserListResource extends JsonResource
{
    /**
     * @param Request|null $request
     * @return array
     */
    public function toArray(Request $request = null): array
    {
        $items = [];
        /** @var LengthAwarePaginator $paginator */
        $paginator = $this->getUsers();

        foreach ($paginator as $user) {
            $items[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone_number,
            ];
        }

        return $items;
    }
    public function getUsers()
    {
        return $this->resource;
    }
}

