<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Requests\User\LoginRequest;
use App\Requests\User\RegisterRequest;
use App\Models\Interfaces\UserInterface;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    protected UserInterface $interface;

    public function __construct(UserInterface $interface)
    {
        $this->interface = $interface;
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        return $this->interface->login($request);
    }

    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function registration(RegisterRequest $request): JsonResponse
    {
       return $this->interface->registration($request);
    }
}
