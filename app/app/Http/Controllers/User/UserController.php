<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Requests\User\UpdateRequest;
use App\Models\Interfaces\UserInterface;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    protected UserInterface $interface;

    public function __construct(UserInterface $interface)
    {
        $this->interface = $interface;
    }

    /**
     * @return JsonResponse
     */
    public function me(): JsonResponse
    {
        return $this->interface->me();
    }

    /**
     * @param int $id
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $id, UpdateRequest $request): JsonResponse
    {
        return response()->json($this->interface->update($id, $request));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        return $this->interface->delete($id);
    }
}
