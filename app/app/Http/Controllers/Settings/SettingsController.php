<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Requests\Settings\SettingsConfirmRequest;
use App\Requests\Settings\SettingsCreateRequest;
use App\Requests\Settings\SettingsUpdateRequest;
use App\Models\Interfaces\SettingsInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

/**
 * @OA\Info (
 * version="1.0.0",
 * title="Settings"
 * ),
 */
class SettingsController extends Controller
{
    protected SettingsInterface $interface;

    public function __construct
    (
        SettingsInterface $interface,
    )
    {
        $this->interface = $interface;
    }

    /**
     * @OA\Get(
     * path="/api/settings/list",
     * operationId="list",
     * tags={"Users"},
     * summary="Settings List",
     * security={{"bearerAuth":{}}},
     * @OA\Response(
     * response=201,
     * description="Successful operation",
     * ),
     * @OA\Response(
     * response=200,
     * description="Successful operation",
     * ),
     * @OA\Response(
     * response=400,
     * description="Bad Request"
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthenticated",
     * ),
     * @OA\Response(
     * response=403,
     * description="Forbidden"
     * )
     * ),
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        return $this->interface->list(
            $request->input('perPage', 18),
            $request->input('page', 1),
        );
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function byId(int $id): JsonResponse
    {
        return $this->interface->byId($id);
    }

    /**
     * @param SettingsCreateRequest $request
     * @return JsonResponse
     */
    public function create(SettingsCreateRequest $request): JsonResponse
    {
       return $this->interface->create($request);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function userBinding(int $id): JsonResponse
    {
       return $this->interface->userBinding($id);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function updateUserBinding(int $id): JsonResponse
    {
       return $this->interface->updateUserBinding($id);
    }

    /**
     * @param int $id
     * @param SettingsConfirmRequest $request
     * @return JsonResponse
     */
    public function confirm(int $id, SettingsConfirmRequest $request): JsonResponse
    {
        if (Cache::get('verification_code') == $request->code) {
            return $this->updateUserBinding($id);
        } else {
            // Сохраняем новый код в кэше
            Cache::put('verification_code', mt_rand(10000, 99999), 600);

            // Отправляем сообщение в зависимости от типа запроса
            if ($request->type == 'sms') {
                $this->interface->sendSms(Cache::get('verification_code'));
            } elseif ($request->type == 'mail') {
                $this->interface->sendEmail(Cache::get('verification_code'));
            } elseif ($request->type == 'telegram') {
                $this->interface->sendTelegramMessage(Cache::get('verification_code'));
            }

            // Возвращаем сообщение пользователю, что новый код был отправлен
            return response()->json(['message' => 'Новый код подтверждения был отправлен'], Response::HTTP_OK);
        }
    }

    /**
     * @param int $id
     * @param SettingsUpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $id, SettingsUpdateRequest $request): JsonResponse
    {
       return $this->interface->update($id, $request);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        return $this->interface->delete($id);
    }
}
