<?php

namespace App\Providers;

use App\Models\Interfaces\SettingsInterface;
use App\Service\Settings\SettingsService;
use App\Service\User\UserService;
use App\Models\Interfaces\UserInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(UserInterface::class, UserService::class);
        $this->app->bind(SettingsInterface::class, SettingsService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
