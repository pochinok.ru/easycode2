<?php

namespace App\Models\Interfaces;

use App\Requests\User\LoginRequest;
use App\Requests\User\RegisterRequest;
use App\Requests\User\UpdateRequest;
use Illuminate\Http\JsonResponse;

interface UserInterface
{
    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse;

    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function registration(RegisterRequest $request): JsonResponse;

    /**
     * @return JsonResponse
     */
    public function me(): JsonResponse;

    /**
     * @param $id
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update($id, UpdateRequest $request): JsonResponse;

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse;
}
