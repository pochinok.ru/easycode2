<?php

namespace App\Models\Interfaces;

use App\Requests\Settings\SettingsCreateRequest;
use App\Requests\Settings\SettingsUpdateRequest;
use Illuminate\Http\JsonResponse;

interface SettingsInterface
{
    /**
     * @param int $perPage
     * @param int $page
     * @return JsonResponse
     */
    public function list(
        int $perPage,
        int $page,
    ): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function byId(int $id): JsonResponse;

    /**
     * @param SettingsCreateRequest $request
     * @return JsonResponse
     */
    public function create(SettingsCreateRequest $request): JsonResponse;

    /**
     * @param int $id
     * @param SettingsUpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $id, SettingsUpdateRequest $request): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function userBinding(int $id): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function updateUserBinding(int $id): JsonResponse;

    /**
     * @param int $code
     * @return void
     */
    public function sendSms(int $code): void;

    /**
     * @param int $code
     * @return void
     */
    public function sendEmail(int $code): void;

    /**
     * @param int $code
     * @return void
     */
    public function sendTelegramMessage(int $code): void;
}
