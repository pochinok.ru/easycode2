<?php

namespace App\Service\Settings;

use App\Mail\SendVerificationCode;
use App\Repository\Settings\SettingsRepository;
use App\Requests\Settings\SettingsCreateRequest;
use App\Requests\Settings\SettingsUpdateRequest;
use App\Resources\Settings\SettingsByIdResource;
use App\Resources\Settings\SettingsPaginatedListResource;
use App\Models\Interfaces\SettingsInterface;
use App\Models\Setting;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SettingsService implements SettingsInterface
{
    protected SettingsRepository $repository;

    public function __construct(
        SettingsRepository $repository,
    )
    {
        $this->repository = $repository;
    }

    /**
     * @param $perPage
     * @param $page
     * @return JsonResponse
     */
    public function list(
        $perPage,
        $page,
    ): JsonResponse
    {
        return response()->json(new SettingsPaginatedListResource($this->repository->paginatedList(
            $perPage,
            $page,
        )));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function byId(int $id): JsonResponse
    {
        return response()->json(new SettingsByIdResource($this->repository->find($id)));
    }

    /**
     * @param SettingsCreateRequest $request
     * @return JsonResponse
     */
    public function create(SettingsCreateRequest $request): JsonResponse
    {
        Setting::create([
            'title' => $request->title,
        ]);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function userBinding(int $id): JsonResponse
    {
        $settings = Setting::whereHas('user', function ($query) {
            $query->where('users.id', Auth::id());
        })->exists();

        if (!$settings) {
                Setting::find($id)->user()->attach(Auth::id(), [
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
                return response()->json([], Response::HTTP_NO_CONTENT);
            } else {
                return response()->json(['У вас уже подключена настройка'], Response::HTTP_ALREADY_REPORTED);
            }
    }

    /**
     * @param int $code
     * @return void
     */
    public function sendSms(int $code): void
    {
        $client = new Client(env('ACCOUNT_SID_TWILLIO'), env('AUTH_TOKEN_TWILLIO'));
        $client->messages->create(
            Auth::user()->phone_number,
            [
                'from' => env('TWILIO_NUMBER'),
                'body' => $code,
            ]
        );
    }

    /**
     * @param int $code
     * @return void
     */
    public function sendEmail(int $code): void
    {
        Mail::to(Auth::user()->email)->send(new SendVerificationCode($code));
    }

    /**
     * @param int $code
     * @return void
     * @throws GuzzleException
     */
    public function sendTelegramMessage(int $code): void
    {
        $telegramBotToken = env('TELEGRAM_BOT_TOKEN');
        $client = new Client();
        $client->get("https://api.telegram.org/bot{$telegramBotToken}/sendMessage", [
            'query' => [
                'chat_id' => env('TELEGRAM_BOT_CHAT_ID'),
                'text' => $code,
            ],
        ]);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function updateUserBinding(int $id): JsonResponse
    {
        $setting = Setting::whereHas('user', function ($query) {
            $query->where('users.id', Auth::id());
        })->first();

        if ($setting) {
            $setting->user()->updateExistingPivot(Auth::id(), ['setting_id' => $id]);
        }

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param int $id
     * @param SettingsUpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $id, SettingsUpdateRequest $request): JsonResponse
    {
        $setting = $this->repository->find($id);

        $setting->update($request->validated());

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
       $this->repository->find($id)->delete();

       return response()->json(['Запись удалена'], Response::HTTP_NO_CONTENT);
    }
}
