<?php

namespace App\Repository\Settings;
use App\Repository\Repository;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class SettingsRepository extends Repository
{
    /**
     *
     * @param Setting $model
     */
    public function __construct(Setting $model)
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginatedList(
        int $perPage,
        int $page,
    ): LengthAwarePaginator
    {
        $query = Setting::query()->orderBy('created_at', 'desc');
        $offset = ($page - 1) * $perPage;
        $results = $query->offset($offset)->limit($perPage)->get();

        return new LengthAwarePaginator($results, $query->count(), $perPage, $page);
    }
}
