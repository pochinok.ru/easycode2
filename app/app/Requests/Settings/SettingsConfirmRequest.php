<?php

namespace App\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class SettingsConfirmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string|in:sms,mail,telegram',
        ];
    }
}
