#!/bin/sh
set -e

# Проверяем доступность порта 5432 на хосте с PostgreSQL
while ! nc -z postgres 5432; do
  sleep 0.1
done

# Выводим сообщение о том, что база данных доступна и выполняем команду
>&2 echo "Database is up - executing command"

# Устанавливаем зависимости Composer
>&2 echo "--- composer install ---"
composer install

# Генерируем ключ приложения Laravel
>&2 echo "--- php artisan key:generate ---"
php artisan key:generate

# Создаем символическую ссылку на хранилище
>&2 echo "--- php artisan storage:link ---"
php artisan storage:link

# Выполняем миграции базы данных
echo "--- php artisan migrate ---"
php artisan migrate

# В этом случае, мы переопределяем команду, чтобы запустить php-fpm
if [ "${1#-}" != "$1" ]; then
        set -- php-fpm "$@"
fi

# Выполняем переданную команду или php-fpm
exec "$@"
