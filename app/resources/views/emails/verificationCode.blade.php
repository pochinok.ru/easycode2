<!DOCTYPE html>
<html>
<style>
</style>
<body >
<div style="border:3px solid cornflowerblue;width: 96%;margin: auto auto;">
    <div style="padding: 1% 1% 1% 1%;width: 98%;">
        <h2>Уважаемый {{$name}}!</h2>
        <div>
            <p style="margin-left: 20px;">Здравствуйте! Ваш код: {{$code}} для подтверждения изменения вашей настройки. Код действителен 10 минут!</p>
            <p style="margin-left: 20px">Если сервер не ответил, вы должны попробовать отправить запрос снова!</p>
        </div>
    </div>
</div>
</body>
</html>
