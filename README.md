# easycode



Запуск окружения:

   ```
   git clone git@gitlab.com:pochinok.ru/easycode.git
   ```
   ```
   docker network create easycode
   ```

   ```
   docker-compose build
   ```

   ```
   docker-compose up -d
   ```

## Database File

The `easycode.sql` file.

